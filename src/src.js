var Player = function(elemento){

	var audio = elemento.find("audio")[0];
  var elemento = elemento;
  var overlay = elemento.find(".overlay");
  var controles = elemento.find(".controls");
  var playing = false;

  this.startAudio = function(){
      audio.ontimeupdate = function(){

      };
      overlay.click($.proxy(function(){
        if(playing){
          this.pause();
        }else{
          this.play();
        }
      },this));

      this.play();
  }

	this.play = function(){
		audio.play();
    playing = true;
    controles.css("background-image","url(./assets/layout/buttons/pause.png)");
	}

  this.pause = function(){
		audio.pause();
    playing = false;
    controles.css("background-image","url(./assets/layout/buttons/play.png)");
	}

}
