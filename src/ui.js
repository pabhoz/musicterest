$(function(){

  var Player = {
      audio: null,
      playing:false,
      controles: null,
      elemento: null,
      currentId: null,
      play: function(){
        if(this.playing){
          pause();
        }else{
          play();
        }
      },
      addListeners: function(){
        Player.elemento = Player.controles.parent().parent();
        console.log("Aplicando Listeners!");
        //this.audio.currentTime = 150;
        this.audio.ontimeupdate = function(){
          var state = (100 / this.duration) * this.currentTime;
          if(state == 100){
            Player.controles.css("background-image","url(./assets/layout/buttons/play.png)");
            Player.playing = false;
          }
          //console.log(state+"%");
          var lbW = Player.elemento.width() * (state/100);
          Player.elemento.find(".loadBar").width(lbW);
        };
      },
      reset: function(){
        console.log("reseteando");
        pause();
        var lb = $(Player.audio).parent().parent().find(".loadBar");
        lb.width(0);
      }
  };

  $.each($(".elemento"),function(i,obj){
    var h = $(obj).height();
    var w = $(obj).width();
    var overlay = $(obj).find(".overlay");
    overlay.height(h);
    overlay.width(w);
    overlay.click(function(){
      //verificación del audio actual
      if($(this).find("audio").data("id") != Player.currentId){

        if(Player.currentId != null) {Player.reset();}

        Player.audio = $(this).find("audio")[0];
        Player.controles = $(this).find(".controls");

        Player.addListeners();
        Player.currentId = $(Player.audio).data("id");

      }
      Player.play();

    });

    var loadBar = $(obj).find(".loadBar");
    loadBar.height(h);
  });

  play = function(){
		Player.audio.play();
    Player.playing = true;
    Player.controles.css("background-image","url(./assets/layout/buttons/pause.png)");
	}

  pause = function(){
		Player.audio.pause();
    Player.playing = false;
    Player.controles.css("background-image","url(./assets/layout/buttons/play.png)");
	}

  $(".elemento").mouseover(function(){
    $(this).find(".overlay").css({
      display:"flex",
      flexFlow: "column wrap",
      justifyContent: "flex-end",
      alignSelf: "flex-end"
    });
  }).mouseleave(function(){
    $(this).find(".overlay").css("display","none");
  });

});
